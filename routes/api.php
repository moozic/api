<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('categories', 'CategoryController@getAll');
Route::get('categories/{id}', 'CategoryController@getById');
Route::post('categories/create', 'CategoryController@store');
Route::post('categories/update', 'CategoryController@update');
Route::delete('categories/{id}', 'CategoryController@delete');

Route::get('artists', 'ArtistController@getAll');
Route::get('artists/{id}', 'ArtistController@getById');
Route::post('artists/create', 'ArtistController@store');
Route::post('artists/update', 'ArtistController@update');
Route::delete('artists/{id}', 'ArtistController@delete');

Route::get('hashtags', 'HashtagController@getAll');
Route::get('hashtags/by/category/{categoryId}', 'HashtagController@getAllTagsByCategory');

Route::get('songs', 'SongController@getAll');
Route::get('songs/category/{categoryId}', 'SongController@getAllByCategory');

Route::group(['middleware' => 'auth:api'], function() {

});
