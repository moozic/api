<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    public function hashtags() {
        return $this->belongsToMany("App\Hashtag")->withTimestamps();
    }

    public function category() {
        return $this->belongsTo("App\Category");
    }

    public function artist(){
        return $this->belongsTo("App\Artist");
    }
}
