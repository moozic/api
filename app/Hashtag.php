<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    public function songs(){
        return $this->belongsTo("App\Song")->withTimestamps();
    }
}
