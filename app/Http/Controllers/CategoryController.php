<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Category;

class CategoryController extends Controller
{
    public function getAll() {
        $categories = Category::all();
        return Response::json($categories, 200);
    }

    public function getById($id) {
        $category = Category::find($id);
        return Response::json($category, 200);
    }

    public function store(Request $request) {
        $category = new Category;
        $category->name = $request->name;
        
        $category->save();

        return Response::json($category, 200);
    }

    public function update(Request $request) {
        $category = Category::find($request->id);
        $category->name = $request->name;
        
        $category->save();

        return Response::json($category, 200);
    }

    public function delete($id) {
        Category::destroy($id);

        return Response::json("Category destroyed", 200);
    }
}
