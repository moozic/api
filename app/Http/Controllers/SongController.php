<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Song;

class SongController extends Controller
{
    public function getAll() {
        $songs = Song::with(['artist'])->paginate(20);
        return Response::json($songs, 200);
    }

    public function getAllByCategory($categoryId) {
        $songs = Song::with(['artist'])->where("category_id",$categoryId)->paginate(5);
        return Response::json($songs, 200);
    }
}
