<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Hashtag;
use App\Song;

class HashtagController extends Controller
{
    public function getAll() {
        $hashtags = Hashtag::all();
        return Response::json($hashtags, 200);
    }

    public function getAllTagsByCategory($categoryId) {
        
        $songs = Song::with(['artist','hashtags'])->where("category_id",$categoryId)->get()->toArray();
        
        $tags = [];

        foreach($songs AS $song):
            foreach($song['hashtags'] AS $tag):
                if(!preg_match('/"'.preg_quote($tag["name"], '/').'"/i' , json_encode($tags))) $tags[] = $tag;
            endforeach;
        endforeach;

        return Response::json($tags, 200);
    }
}
