<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "lastname"=>"NEYMARDEVOUS",
                "firstname"=>"Jean",
                "email"=>"jeannaymardevous"
            ],
            [
                "lastname"=>"CROCHET",
                "firstname"=>"Sarah",
                "email"=>"sarachcrochet"
            ],
            [
                "lastname"=>"ESTJAUNE",
                "firstname"=>"Gilles",
                "email"=>"gillesestjaune"
            ]
        ];

        foreach($users AS $key=>$user):
            $user["image"] = "user". $key . ".jpg";
            $user["password"] = hash::make("secret"); //"secret";
            $user["email"] .= "@test.com";

            User::create($user);
        endforeach;
    }
}
