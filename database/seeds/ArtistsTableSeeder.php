<?php

use Illuminate\Database\Seeder;
use App\Artist;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $artists = [
            ["name"=>"Benjamin Tissot"],
            ["name"=>"Alain TERRIEUR"],
            ["name"=>"Alex TERRIEUR"],
        ];

        foreach($artists AS $key=>$artist):
            $artist["image"] = "artist". $key . ".jpg";
            Artist::create($artist);
        endforeach;
    }
}
