<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            ["name"=>"ACOUSTIC/FOLK"],
            ["name"=>"CINEMATIC"],
            ["name"=>"CORPORATE/TOP"],
            ["name"=>"ELECTRONICA"],
            ["name"=>"URBAN/GROOVE"],
            ["name"=>"JAZZ"],
            ["name"=>"ROCK"],
            ["name"=>"WORLD/OTHERS"],
        ];

        foreach($categories AS $key=>$categorie):
            Category::create($categorie);
        endforeach;
    }
}
